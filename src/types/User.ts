type Gender = 'male' | 'female' | 'other'
type Role = 'admin' | 'user'
type User = {
  id: number
  email: string
  password: string
  fullName: string
  gender: Gender //Male, Female ,Other
  roles: Role[] //addmin,user
}
export type {Gender,Role,User}